import type { Config } from "@jest/types";

export default (): Config.InitialOptions => {
  return {
    testEnvironment: "jsdom",
    setupFilesAfterEnv: ["<rootDir>/.jest/jest.setup.ts"],
    testPathIgnorePatterns: ["<rootDir>/lib/", "<rootDir>/node_modules/", "<rootDir>/dist/"],
    roots: ["./src"],
    collectCoverageFrom: [
      "src/**/*.{ts,tsx}",
      "!src/**/*stories.tsx",
      "!src/**/types.ts",
      "!src/**/*.d.ts",
      "!src/index.ts",
      "!src/types/*",
      "!src/showcase/*",
      "!src/helpers/storybook.tsx",
    ],
    moduleNameMapper: {
      "@/(.*)": "<rootDir>/src/$1",
      "\\.(scss|sass|css)$": "identity-obj-proxy",
    },
  };
};
