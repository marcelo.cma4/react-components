import { MouseEventHandler } from "react";
import classNames from "classnames";

import { Spacing } from "@/types/layout";
import { FontSize, FontWeight } from "@/types/text";
import { getMargin } from "@/helpers/styles";
import { BrColor } from "@/types/colors";

type ElementType = "div" | "span" | "p" | "h1" | "h2" | "h3" | "h4" | "h5" | "h6";

export type TypographyProps = {
  children: React.ReactNode;
  className?: string;
  id?: string;
  size?: FontSize;
  weight?: FontWeight;
  color?: BrColor;
  margin?: Spacing;
  htmlElement?: ElementType;
  onClick?: MouseEventHandler<HTMLSpanElement>;
};

const Typography = (props: TypographyProps) => {
  const { size, weight, color, className, children, margin, htmlElement, ...rest } = props;

  const bgHighlight = color || "default";

  const classes = classNames(className, getMargin(margin), {
    [`text-${size}`]: !!size,
    [`text-${weight}`]: !!weight,
    [`text-${color}`]: !!color,
    [`ebr-clickable-${bgHighlight}`]: !!rest.onClick,
    "ebr-clickable-text": !!rest.onClick,
  });

  const Wrapper = htmlElement || "div";

  return (
    <Wrapper className={classes} {...rest}>
      {children}
    </Wrapper>
  );
};

export default Typography;
