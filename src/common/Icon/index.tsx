import classNames from "classnames";

import Container from "@/common/Container";
import { Col, Row } from "@/common/Grid";

import { IconSize } from "@/types/icons";

type BadgeProps = {
  badge: React.ReactNode;
  children: React.ReactNode;
};

interface Margin {
  l: number;
  r: number;
}

type IconProps = {
  icon: string;
  size?: IconSize;
  badge?: React.ReactNode;
  margin?: Margin;
};

const Badge = ({ badge, children }: BadgeProps) => {
  return (
    <Container fluid margin={0} padding={0}>
      <Row>
        <Col sm="auto" padding={{ r: 0 }}>
          {children}
        </Col>
        <Col sm="auto" padding={{ l: 0 }}>
          {badge}
        </Col>
      </Row>
    </Container>
  );
};

const Content = ({ icon, size, ...rest }: IconProps) => {
  const svg = icon.endsWith(".svg");
  const classes = classNames("fas", size && `fa-${size}`, !svg && `fa-${icon}`);

  return svg ? (
    <img className={classes} src={icon} {...rest} />
  ) : (
    <i className={classes} {...rest} />
  );
};

const Icon = ({ badge, ...rest }: IconProps) => {
  return badge ? (
    <Badge badge={badge}>
      <Content {...rest} />
    </Badge>
  ) : (
    <Content {...rest} />
  );
};

export default Icon;
