export type Breadcrumb = {
  isHome?: boolean;
  label: string;
  onClick: React.MouseEventHandler<HTMLButtonElement>;
};

export type BreadcrumbsProps = {
  crumbs?: Breadcrumb[];
};

const HomeCrumb = ({ label, onClick }: Breadcrumb) => {
  return (
    <li className="crumb home">
      <span className="sr-only">{label}</span>
      <a>
        <i className="icon fas fa-home" onClick={onClick} />
      </a>
    </li>
  );
};

const RouteCrumb = ({ label, onClick }: Breadcrumb) => {
  return (
    <li className="crumb">
      <i className="text-center icon fas fa-chevron-right" />
      <a>
        <span onClick={onClick}>{label}</span>
      </a>
    </li>
  );
};

const Crumb = ({ isHome, ...rest }: Breadcrumb) => {
  return isHome ? <HomeCrumb {...rest} /> : <RouteCrumb {...rest} />;
};

const BrBreadcrumbs = ({ crumbs = [] }: BreadcrumbsProps) => {
  return (
    <div className="br-breadcrumb pt-3">
      <ul className="crumb-list">
        {crumbs.map((crumb, index) => (
          <Crumb key={index} {...crumb} />
        ))}
      </ul>
    </div>
  );
};

export default BrBreadcrumbs;
