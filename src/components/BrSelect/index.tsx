import { MouseEvent, MouseEventHandler, useState } from "react";
import classNames from "classnames";
import uniqueId from "lodash/uniqueId";

import Icon from "@/common/Icon";

import BrButton from "@/components/BrButton";
import BrItem from "@/components/BrItem";
import BrList from "@/components/BrList";

export type BrSelectProps = {
  label?: string;
  placeholder?: string;
  name?: string;
  options: string[];
  value: string;
  onClick: MouseEventHandler<HTMLInputElement>;
};

const BrSelect = ({ label, placeholder, name, options, value, onClick }: BrSelectProps) => {
  const selectId = uniqueId("select-");
  const [expanded, setExpanded] = useState<string | null>(null);
  const toggleExpanded = () => setExpanded(expanded ? null : "expanded");
  const [selectValue, setSelectValue] = useState(value);

  const handleClick = (event: MouseEvent<HTMLInputElement>) => {
    onClick(event);
    setSelectValue(event.currentTarget.value);
    setExpanded(null);
  };

  return (
    <div className="br-select">
      <div className="br-input">
        {label && <label htmlFor={selectId}>{label}</label>}

        <input id={selectId} type="text" value={selectValue} placeholder={placeholder} />
        <BrButton
          circle
          size="small"
          aria-label="Exibir lista"
          tabIndex={-1}
          onClick={toggleExpanded}
        >
          <Icon icon={expanded ? "angle-up" : "angle-down"} />
        </BrButton>
      </div>
      <BrList tabIndex={0} expanded={expanded}>
        {options &&
          options.map((opt) => (
            <BrItem
              key={opt}
              tabIndex={-1}
              className={classNames(opt === selectValue && "selected")}
            >
              <div className="br-radio">
                <input
                  id={`id-item-${opt}`}
                  type="radio"
                  name={name}
                  value={opt}
                  onClick={handleClick}
                />
                <label htmlFor={`id-item-${opt}`}>{opt}</label>
              </div>
            </BrItem>
          ))}
      </BrList>
    </div>
  );
};

export default BrSelect;
