import { Story, Meta } from "@storybook/react";

import BrSelect, { BrSelectProps } from "@/components/BrSelect";

export default {
  title: "input/BrSelect",
  component: BrSelect,
  args: {
    label: "",
    placeholder: "Clique aqui",
    options: ["opção 1", "opção 2", "opção 3"],
  },
} as Meta;

const Template: Story<BrSelectProps> = (args) => <BrSelect {...args} />;

export const Simples = Template.bind({});

export const ComRotulo = Template.bind({});
ComRotulo.args = { ...ComRotulo.args, label: "Rótulo do Select" };
