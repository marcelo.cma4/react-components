import { Story, Meta } from "@storybook/react";

import BrTextarea, { BrTextareaProps } from ".";

export default {
  title: "Input/BrTextarea",
  component: BrTextarea,
} as Meta;

export const Simples: Story<BrTextareaProps> = (args) => (
  <BrTextarea {...args}>Conteúdo</BrTextarea>
);
