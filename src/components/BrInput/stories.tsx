import { Story, Meta } from "@storybook/react";

import BrInput, { BrInputProps } from ".";

import { Col, Row } from "@/common/Grid";
import { useState } from "react";
import { MessageType } from "@/types/messages";

export default {
  title: "Input/BrInput",
  component: BrInput,
} as Meta;

const Template: Story<BrInputProps> = (args) => (
  <Row>
    <Col sm={3}>
      <BrInput {...args} />
    </Col>
  </Row>
);

export const Simples = Template.bind({});
Simples.args = {
  label: "Label Customizado",
  placeholder: "Placeholder customizado",
};

export const ComFeedback = Template.bind({});
ComFeedback.args = {
  label: "Label Customizado",
  placeholder: "Placeholder customizado",
  feedback: [{ type: "danger" as MessageType, message: "Campo Obrigatório" }],
};
