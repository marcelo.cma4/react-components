import React from "react";
import classNames from "classnames";
import { uniqueId } from "lodash";

import BrContextMessage, { BrContextMessageProps } from "../BrContextMessage";

export type BrInputProps = {
  className?: string;
  children?: React.ReactNode;
  id?: string;
  label?: string;
  placeholder?: string;
  type?: "text" | "number" | "radio" | "time" | "datetime-local";
  readOnly?: boolean;
  hasIcon?: boolean;
  density?: "small" | "medium" | "large";
  feedback?: BrContextMessageProps[];
  value?: string | number;
  onClick?: React.MouseEventHandler<HTMLInputElement>;
  onChange?: React.ChangeEventHandler<HTMLInputElement>;
};

const BrInput = React.forwardRef<HTMLInputElement, BrInputProps>((props, ref) => {
  const {
    id = uniqueId("input-"),
    label,
    type = "text",
    hasIcon,
    density,
    feedback,
    className,
    children,
    ...rest
  } = props;

  const classes = classNames(
    className,
    type === "radio" ? "br-radio" : "br-input",
    !!density && density,
    { "has-icon": hasIcon }
  );
  const inputClasses = classNames(className, { "has-icon": hasIcon });

  return (
    <div className={classes}>
      {label && <label htmlFor={id}>{label}</label>}
      <input className={inputClasses} id={id} type={type} {...rest} ref={ref} aria-label={label} />
      {children}
      {feedback?.map((item, index) => (
        <div className="mt-1" key={index}>
          <BrContextMessage {...item} />
        </div>
      ))}
    </div>
  );
});

BrInput.displayName = "BrInput";
export default BrInput;
