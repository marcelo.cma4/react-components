import classNames from "classnames";

export type BrItemProps = {
  children: React.ReactNode;
  className?: string;
  dataToggle?: string;
  disabled?: boolean;
  role?: "listitem";
  divider?: boolean;
  density?: "media" | "baixa";
  isLabel?: boolean;
  tabIndex?: 0 | -1;
};

const BrItem = (props: BrItemProps) => {
  const { children, className, density, divider, ...rest } = props;

  const classes = classNames("br-item", "align-items-center", className, {
    "py-3": density === "media",
    "py-4": density === "baixa",
  });

  return (
    <>
      <div className={classes} {...rest}>
        {children}
      </div>
      {divider && (
        <>
          <span className="br-divider"></span>
        </>
      )}
    </>
  );
};

export default BrItem;
