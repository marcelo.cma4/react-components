import { Story, Meta } from "@storybook/react";
import BrCheckbox from "@/components/BrCheckbox";

import BrItem, { BrItemProps } from ".";

export default {
  title: "Superfícies/BrItem",
  component: BrItem,
  args: {
    divider: false,
    children: "item x",
  },
} as Meta;

const Template: Story<BrItemProps> = (args) => {
  return <BrItem {...args} />;
};

export const Simples = Template.bind({});

export const ComCheck = Template.bind({});
ComCheck.args = {
  ...ComCheck.args,
  divider: true,
  children: <BrCheckbox label="item x" />,
};
