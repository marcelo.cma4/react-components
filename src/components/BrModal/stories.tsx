import { Story, Meta } from "@storybook/react";
import BrButton from "@/components/BrButton";
import { useState } from "react";

import BrModal, { BrModalProps } from ".";

export default {
  title: "Utilidades/BrModal",
  component: BrModal,
} as Meta;

const Template: Story<BrModalProps> = (args) => {
  const [open, setOpen] = useState(true);

  return (
    <>
      <BrButton primary onClick={() => setOpen(true)}>
        Abrir Modal
      </BrButton>
      <BrModal {...args} isOpen={open} onClose={() => setOpen(false)}>
        Elementos que compõem o conteúdo do modal. Definem a largura e a altura do mesmo.
      </BrModal>
    </>
  );
};

export const Simples = Template.bind({});

export const ComActionPrincipal = Template.bind({});
ComActionPrincipal.args = {
  title: "Título do Modal",
  primaryAction: { label: "Ok", action: () => "", disabled: true },
};

export const ComActions = Template.bind({});
ComActions.args = {
  title: "Título do Modal",
  primaryAction: { action: () => "" },
  secondaryAction: { action: () => "" },
};
