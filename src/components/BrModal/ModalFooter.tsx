import classNames from "classnames";
import BrButton from "@/components/BrButton";
import { MouseEventHandler } from "react";
import { BrModalActions } from ".";

type ButtonProps = {
  type?: "submit" | "reset" | "button";
  onClick?: MouseEventHandler<HTMLButtonElement>;
};

const ModalFooter = ({ primaryAction, secondaryAction, onClose, formId }: BrModalActions) => {
  const justify = !!primaryAction && !!secondaryAction ? "end" : "center";
  const classes = classNames("br-modal-footer px-2", `justify-content-${justify}`);

  const handlePrimary = () => {
    primaryAction?.action && primaryAction.action();
    onClose();
  };

  const handleSecondary = () => {
    secondaryAction?.action && secondaryAction.action();
    onClose();
  };

  const buttonProps: ButtonProps = formId
    ? { type: "submit" }
    : { type: "button", onClick: handlePrimary };

  return primaryAction || secondaryAction ? (
    <div className={classes}>
      {!!secondaryAction && (
        <BrButton
          secondary
          size="small"
          margin={{ t: 1, l: 1 }}
          onClick={handleSecondary}
          disabled={secondaryAction.disabled}
          block
          clearBlock="md"
        >
          {secondaryAction.label || "Cancelar"}
        </BrButton>
      )}
      {!!primaryAction && (
        <BrButton
          primary
          size="small"
          margin={{ t: 1, l: 1 }}
          disabled={primaryAction.disabled}
          block
          clearBlock="md"
          {...buttonProps}
        >
          {primaryAction.label || "Confirmar"}
        </BrButton>
      )}
    </div>
  ) : null;
};

export default ModalFooter;
