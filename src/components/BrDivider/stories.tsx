import { Story, Meta } from "@storybook/react";

import BrDivider, { BrDividerProps } from ".";

export default {
  title: "Utilidades/BrDivider",
  component: BrDivider,
} as Meta;

export const Horizontal: Story<BrDividerProps> = () => (
  <div>
    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
    <BrDivider margin={{ t: 3, b: 3 }} />
    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit.</p>
  </div>
);

export const Vertical: Story<BrDividerProps> = () => (
  <div className="d-flex">
    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
    <BrDivider margin={{ l: 3, r: 3 }} vertical={true} />
    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit.</p>
  </div>
);
