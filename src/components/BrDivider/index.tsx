import classNames from "classnames";

import { getMargin } from "@/helpers/styles";
import { Spacing } from "@/types/layout";

export type BrDividerProps = {
  margin?: Spacing;
  vertical?: boolean;
};

const BrDivider = (props: BrDividerProps) => {
  const { margin, vertical } = props;

  const classes = classNames("br-divider", getMargin(margin), { vertical });

  return <span className={classes}></span>;
};

export default BrDivider;
