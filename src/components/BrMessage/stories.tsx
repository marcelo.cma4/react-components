import { Col, Row } from "@/common/Grid";
import { Story, Meta } from "@storybook/react";
import { useState } from "react";

import BrMessage, { BrMessageProps } from ".";

export default {
  title: "Feedback/BrMessage",
  component: BrMessage,
} as Meta;

export const Default = () => {
  return (
    <Row>
      <Col sm={12}>
        <BrMessage
          type="success"
          message="Lorem, ipsum dolor sit amet consectetur adipisicing elit. Minus, a!"
        />
      </Col>
      <Col sm={12}>
        <BrMessage
          type="info"
          message="Lorem, ipsum dolor sit amet consectetur adipisicing elit. Minus, a!"
        />
      </Col>
      <Col sm={12}>
        <BrMessage
          type="warning"
          message="Lorem, ipsum dolor sit amet consectetur adipisicing elit. Minus, a!"
        />
      </Col>
      <Col sm={12}>
        <BrMessage
          type="danger"
          message="Lorem, ipsum dolor sit amet consectetur adipisicing elit. Minus, a!"
        />
      </Col>
    </Row>
  );
};

export const ComTitulo: Story<BrMessageProps> = (args) => <BrMessage {...args} />;
ComTitulo.args = {
  title: "Título.",
  inlineTitle: true,
  type: "success",
  message: "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Minus, a!",
  onClose: undefined,
};

export const OnClose = () => {
  const [showMessage, setShowMessage] = useState(true);

  return (
    showMessage && (
      <BrMessage
        title="Título."
        inlineTitle
        type="success"
        message="Lorem, ipsum dolor sit amet consectetur adipisicing elit. Minus, a!"
        onClose={() => setShowMessage(false)}
      />
    )
  );
};
