import { render } from "@testing-library/react";
import BrContextMessage from ".";

describe("<BrContextMessage />", () => {
  it("snapshot test", () => {
    const { container } = render(
      <BrContextMessage type="success" message="Mensagem de contexto de sucesso" />
    );
    expect(container.firstChild).toMatchSnapshot();
  });

  it("deveria renderizar context message sucesso", () => {
    const { container } = render(<BrContextMessage type="success" message="Mensagem de sucesso" />);
    expect(container.firstChild).toHaveClass("feedback success");
  });

  it("deveria renderizar context message sucesso", () => {
    const { container } = render(<BrContextMessage type="success" message="Mensagem de sucesso" />);
    expect(container.firstChild).toHaveClass("feedback success");
  });

  it("deveria renderizar context message info", () => {
    const { container } = render(<BrContextMessage type="info" message="Mensagem de info" />);
    expect(container.firstChild).toHaveClass("feedback info");
  });

  it("deveria renderizar context message warning", () => {
    const { container } = render(<BrContextMessage type="warning" message="Mensagem de alerta" />);
    expect(container.firstChild).toHaveClass("feedback warning");
  });

  it("deveria renderizar context message danger", () => {
    const { container } = render(<BrContextMessage type="danger" message="Mensagem de erro" />);
    expect(container.firstChild).toHaveClass("feedback danger");
  });
});
