import { Col, Row } from "@/common/Grid";
import Typography from "@/common/Typography";
import { Story, Meta } from "@storybook/react";

import BrContextMessage from ".";

export default {
  title: "Feedback/BrContextMessage",
  component: BrContextMessage,
} as Meta;

export const Default = () => (
  <>
    <Row>
      <Col>
        <BrContextMessage type="success" message="Mensagem de Sucesso" />
      </Col>
      <Col>
        <BrContextMessage type="info" message="Mensagem Informativa" />
      </Col>
      <Col>
        <BrContextMessage type="warning" message="Mensagem de Alerta" />
      </Col>
      <Col>
        <BrContextMessage type="danger" message="Mensagem de Erro" />
      </Col>
    </Row>
    <Typography margin={{ t: 5 }}>
      Veja uso de feedback contexto para validação na documentação de componentes de input, como{" "}
      <b>BrInput</b> ou <b>BrCheckboxGroup</b>.
    </Typography>
  </>
);
