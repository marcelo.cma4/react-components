import { Story, Meta } from "@storybook/react";
import Icon from "@/common/Icon";
import BrButton, { BrButtonProps } from ".";

export default {
  title: "Input/BrButton",
  component: BrButton,
} as Meta;

export const Simples: Story<BrButtonProps> = (args) => <BrButton {...args}>Rótulo</BrButton>;

export const Primary: Story<BrButtonProps> = (args) => <BrButton {...args}>Rótulo</BrButton>;
Primary.args = { ...Primary.args, primary: true };

export const Secondary: Story<BrButtonProps> = (args) => <BrButton {...args}>Rótulo</BrButton>;
Secondary.args = { ...Secondary.args, secondary: true };

export const Circle: Story<BrButtonProps> = ({ ...args }) => <BrButton {...args}>R</BrButton>;
Circle.args = { ...Circle.args, circle: true, primary: true };

export const ComIcone: Story<BrButtonProps> = (args) => (
  <BrButton {...args}>
    Cancelar
    <Icon icon="times" margin={{ l: 1, r: -1 }} />
  </BrButton>
);
ComIcone.args = { ...ComIcone.args, primary: true };
