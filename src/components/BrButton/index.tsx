import classNames from "classnames";

import { getMargin } from "@/helpers/styles";
import { forwardRef } from "react";
import { ClearBlock, Spacing } from "@/types/layout";

export interface BrButtonProps extends React.ComponentPropsWithoutRef<"button"> {
  loading?: boolean;
  primary?: boolean;
  secondary?: boolean;
  circle?: boolean;
  close?: boolean;
  size?: "small" | "large";
  margin?: Spacing;
  block?: boolean;
  clearBlock?: ClearBlock;
}

const BrButton = forwardRef<HTMLButtonElement, BrButtonProps>((props, ref) => {
  const {
    children,
    className,
    loading,
    circle,
    primary,
    secondary,
    close,
    size,
    margin,
    block,
    clearBlock,
    type = "button",
    ...rest
  } = props;

  const classes = classNames("br-button", className, getMargin(margin), {
    loading,
    circle,
    primary,
    secondary: !primary && secondary,
    large: size === "large",
    small: size === "small",
    close,
    block,
    [`auto-${clearBlock}`]: !!clearBlock,
  });

  return (
    <button ref={ref} className={classes} type={type} {...rest}>
      {children}
    </button>
  );
});

BrButton.displayName = "BrButton";
export default BrButton;
