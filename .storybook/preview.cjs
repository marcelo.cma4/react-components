import "@govbr-ds/core/dist/core.min.css";
import "@fortawesome/fontawesome-free/css/all.min.css";

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
};
